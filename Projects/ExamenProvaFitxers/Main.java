import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;

public class Main {

	public static void main(String[] args) throws IOException, ClassNotFoundException {
		
		ex1();
		ex2();
		
	}
	private static void ex2() throws IOException, ClassNotFoundException {
		//LECTURA TEXTO
		File f = new File("gats.txt");
		FileReader in = new FileReader(f);
		BufferedReader br = new BufferedReader(in);
		
		String nom;
	    String raza;
	    int edat;
	    String menjarPreferit;
	    boolean capat;
	    ArrayList<Gat> gatitos = new ArrayList<Gat>();
	    while(br.ready()) {
	    nom=br.readLine();
	    System.out.println(nom);
	    raza=br.readLine();
	    System.out.println(raza);
	    edat=Integer.parseInt(br.readLine());
	    menjarPreferit=br.readLine();
	    capat=Boolean.parseBoolean(br.readLine());
	    Gat g = new Gat (nom, raza, edat, menjarPreferit, capat);
	    gatitos.add(g);
	    }
	    br.close();
	    	    
	    //ESCRITURA SERIE
	    File f2 = new File("gats.dat");
		FileOutputStream fos = new FileOutputStream(f2);
		ObjectOutputStream oos = new ObjectOutputStream(fos);
		for(Gat gato : gatitos) {
		oos.writeObject(gato);
		}
		oos.flush();
		oos.close();
		
		//LECTURA SERIE
		
	    File f3 = new File("gats.dat");
		FileInputStream fis = new FileInputStream(f3);
		ObjectInputStream ois = new ObjectInputStream(fis);
		
		for (Gat gat : gatitos) {
			Object o = ois.readObject();
			Gat g2 = (Gat) o;
			System.out.println(g2);
		}
		
		
		///ESCRIBIR A FICHERO OTRA VEZ
		File f4 = new File("gats2.txt");
		FileWriter out = new FileWriter(f4);
		BufferedWriter bw = new BufferedWriter(out);
		
		bw.write(g2.nom);
		bw.newLine();
		bw.write(g2.raza);
		bw.newLine();
		bw.write(g2.edat+"");
		bw.newLine();
		bw.write(g2.menjarPreferit);
		bw.newLine();
		bw.write(g2.capat+"");
		bw.newLine();
		
		bw.flush();
		bw.close();
		
		
		
		
		
		
	}
		
	private static void ex1() throws IOException, ClassNotFoundException {
		//LECTURA TEXTO
				File f = new File("gat.txt");
				if(!f.exists()) f.createNewFile();
				FileReader in = new FileReader(f);
				BufferedReader br = new BufferedReader(in);
				
				String nom;
			    String raza;
			    int edat;
			    String menjarPreferit;
			    boolean capat;
			    
			    nom=br.readLine();
			    System.out.println(nom);
			    raza=br.readLine();
			    System.out.println(raza);
			    edat=Integer.parseInt(br.readLine());
			    menjarPreferit=br.readLine();
			    capat=Boolean.parseBoolean(br.readLine());
			    br.close();
			    
			    Gat g = new Gat (nom, raza, edat, menjarPreferit, capat);
			    System.out.println(g);
			    
			    //ESCRITURA SERIE
			    File f2 = new File("gat.dat");
				FileOutputStream fos = new FileOutputStream(f2);
				ObjectOutputStream oos = new ObjectOutputStream(fos);
				
				oos.writeObject(g);
				
				oos.flush();
				oos.close();
				
				
				
				//LECTURA SERIE
				
			    File f3 = new File("gat.dat");
				FileInputStream fis = new FileInputStream(f3);
				ObjectInputStream ois = new ObjectInputStream(fis);
				
				
				Object o = ois.readObject();
				Gat g2 = (Gat) o;
				System.out.println(g2);
				
				///ESCRIBIR A FICHERO OTRA VEZ
				File f4 = new File("gat2.txt");
				FileWriter out = new FileWriter(f4);
				BufferedWriter bw = new BufferedWriter(out);
				
				bw.write(g2.nom);
				bw.newLine();
				bw.write(g2.raza);
				bw.newLine();
				bw.write(g2.edat+"");
				bw.newLine();
				bw.write(g2.menjarPreferit);
				bw.newLine();
				bw.write(g2.capat+"");
				bw.newLine();
				
				bw.flush();
				bw.close();
				
				
				
				
				
				
			}
		
}
