/**
 * Clase Proyectil que extiende de Sprite y representa los proyectiles que lanza Ragnar
 * @author David Clemente
 * @version 1.0
 *
 */
public class Proyectil extends Proyectiles {

	char direccion;
	
	public Proyectil(String name, int x1, int y1, int x2, int y2, String path) {
		super(name, x1, y1, x2, y2, path);
		// TODO Auto-generated constructor stub
	}

	public Proyectil(String name, int x1, int y1, int x2, int y2, String[] cambioImagenProyectil, char dir) {
		super(name, x1, y1, x2, y2, cambioImagenProyectil, dir);
		direccion = dir;
		
	}
	/**
	  * Mira si el proyectil de nuestro personaje ha colisionado con algo y si ha colisionado lo borra
	  */
	public void colision (Field f) {
		Sprite s = firstCollidesWithField(f);
				//si ha colisionado con algo
				if(s!=null) {
				//si s es de Clase Enemigo.
					if(s instanceof Enemigos) {
						//casteo de enemigo
						Enemigos en = (Enemigos) s;
						en.Danado();
					}
					if(!(s instanceof Ragnar)) {
						delete();
					}
				}
	}
	/**
	  * Movimiento del proyectil de nuestro personaje
	  */
	public void move(Field f) {
		if(direccion=='a') {
			x1-=15;x2-=15;
		}else {
			x1+=15;x2+=15;
		}
		colision(f);
	}

}
