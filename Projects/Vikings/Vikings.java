
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Random;
import java.util.Scanner;

/**
 * Juego de plataforma con tematica de Vikingos donde puedes saltar, ir hacia la
 * izquierda, ir hacia la derecha, transformarte en cuervo y lanzar proyectiles.
 * 
 * @author David Clemente
 * @version 1.0
 *
 */
public class Vikings {
	/**
	 * booleano que determina si nuestro personaje se transforma en cuervo o no
	 */
	static boolean trans = false;
	static boolean jugar = false;
	static boolean ins = false;
	static boolean salir = false;
	static boolean portal = false;
	static boolean apple = false;
	static boolean jugar2 = false;
	static boolean jugar3 = false;
	static boolean salirjugar= false;
	/**
	 * booleano que determina si el juego sigue o se acaba
	 */
	static boolean flag = false;
	static Field f = new Field();
	static Window w = new Window(f);
	static Scanner sc = new Scanner(System.in);
	/**
	 * Array de Strings donde estan todos los Sprites de nuestro personaje
	 */
	static String[] cambioImagen = { "", "RagnarDerecha.png", "RagnarIzquierda.png", "EspadazoDerecha.png",
			"EspadazoIzquierda.png", "Escudo.png", "super.gif", "cuervo.gif", "RagnarAgachado!.png",
			"RagnarAgachadoIzquierda.png" };
	static String[] vidas = {"Vidas3.png", "Vidas2.png", "Vidas1.png", ""};
	static String[] suelitos = {"SueloTierra.png", "soylabomba.png"};
	static Barquito b = new Barquito("Barco", 0, 500, 270, 700, "barco.png");

	/**
	 * Declaracion de nuestro personaje con el nombre, x1, y1, x2, y2, y el Array de
	 * Strings de sus imagenes
	 */
<<<<<<< HEAD
	static Ragnar r = new Ragnar("Ragnar", 100, 250, 250, 400, cambioImagen);
	static Mapas m = new Mapas(); 
	static ArrayList<Sprite> menu = new ArrayList<>();
=======
//	static Ragnar r = new Ragnar("Ragnar", 100, 250, 250, 400, cambioImagen);
	static Mapas m = new Mapas();
	static ArrayList<Botones> menu = new ArrayList<>();
>>>>>>> branch 'master' of https://gitlab.com/Clemente43/projects.git
	static HUD hud = new HUD("vidas", 25, 25, 175, 75, vidas);
	static Random random = new Random();
	static String[] musicas = { "", "" };
	static int cancionAct = 0;
	static int minscroll = 250;
	static int maxscroll = 300;

	public static void main(String[] args) throws InterruptedException {
		int contador = 0;
//		int opcion = menu();
//		while(!salir) {
//		switch (opcion) {
//		case 1:
//			listasVacias();
//			jugar();
//			Thread.sleep(500);
//			break;
//		case 2:
//			listasVacias();
//				instrucciones();
//				Thread.sleep(500);
//			break;
//		default:
//			break;
//		}
//	}
//}
		while(!salir) {
			listasVacias();
		menu();
		while(!salirjugar) {
		if (jugar) {
			listasVacias();
<<<<<<< HEAD
			jugar3();
=======
			jugar();
>>>>>>> branch 'master' of https://gitlab.com/Clemente43/projects.git
		
		}else if (jugar2) {
			listasVacias();
			jugar2();
<<<<<<< HEAD
		}else if (jugar3) {
			listasVacias();
			jugar3();
=======
			
>>>>>>> branch 'master' of https://gitlab.com/Clemente43/projects.git
		}
		}
	}
	}
	private static void jugar3() throws InterruptedException {
		r.useImgArray = true;
		hud.useImgArray= true;
//		w.playMusic("");
		r.currentImg = 1;
		jugar = false;
		for(Terreno t : Vikings.m.tierras) {
		t.currentImg = 0;
		}
		r = new Ragnar("Ragnar", 100, 250, 230, 400, cambioImagen);
		hud = new HUD("vidas", 25, 25, 175, 75, vidas);
		m.enemigos = new ArrayList<>();
		m.tierras = new ArrayList<>();
		m.tiros = new ArrayList<>();
		m.tirosE = new ArrayList<>();
		trans = false;
		jugar = false;
		jugar2 = false;
		ins = false;
		salir = false;
		portal = false;
		apple = false;
		flag = false;
		m.enemigos.add((new Enemigos("Pato", 1850, 550, 1950, 620, "Ene.png")));
		m.enemigos.add((new Enemigos("Pato", 4150, 550, 4250, 620, "Ene.png")));
		m.enemigos.add((new Enemigos("Pato", 5850, 550, 5950, 620, "Ene.png")));
		m.enemigos.add((new Enemigos("PatoF", 7800, 400, 7950, 620, "Ene.png")));
//		m.tierras.add((new Terreno("suelo", 100, 400, 600, 470, "SueloTierra.png")));
//		m.tierras.add((new Terreno("suelo", 400, 600, 1000, 650, "SueloTierra.png")));
		m.tierras.add((new Terreno("suelo", 1000, 650, 1170, 700, suelitos)));
		m.tierras.add((new Terreno("sueloabajo", 1170, 650, 1400, 700, suelitos)));
		m.tierras.add((new Terreno("suelo", 1400, 650, 2000, 700, suelitos)));
		m.tierras.add((new Terreno("sueloabajo2", 4000, 650, 4300, 700, suelitos)));
		m.tierras.add((new Terreno("suelo", 3400, 650, 4000, 700, suelitos)));
		m.tierras.add((new Terreno("suelo", 5200, 650, 5390, 700, suelitos)));
		m.tierras.add((new Terreno("suelopincho", 5390, 650, 5400, 700, suelitos)));
		m.tierras.add((new Terreno("suelo", 5400, 650, 6000, 700, suelitos)));
		m.tierras.add((new Terreno("suelo", 7400, 650, 8000, 700, suelitos)));
		m.tierras.add((new Terreno("suelo", 8100, 650, 8300, 700, suelitos)));		
		
		
		
		while (!flag) {
			f.background = "fondobikini.jpg";
			r.choque();
			Enemigos.moveE();
			r.update();
			Enemigos.disparoE();
			patitoMuerto();
			input();
			ProyectilE.colisionP();
			Enemigos.saltoE();
			r.marcadorV(hud);
			Terreno.nyamnyam();
			r.SinGravedad();
			r.RagnarTocao();
			GoPortal();
			GoApple();
			ArrayList<Sprite> sprites = new ArrayList<Sprite>();
			sprites.addAll(m.enemigos);
			sprites.add(r);
			sprites.addAll(m.tierras);
			sprites.addAll(m.tiros);
			sprites.addAll(m.tirosE);
			sprites.add(hud);
			for (Iterator<Sprite> iterator = sprites.iterator(); iterator.hasNext();) {
				Sprite s = (Sprite) iterator.next();
				if (s.delete) {
					iterator.remove();
				}
			}
			f.draw(sprites);

			Thread.sleep(20);

		}
		}
		
	
	private static void jugar2() throws InterruptedException {
		Ragnar r = Ragnar.getRagnar();
		r.x1= 100;
		r.x2=250;
		r.y1=450;
		r.y2=600;
		r.hp=3;
		r.useImgArray = true;
		hud.useImgArray= true;
//		w.playMusic("");
		r.currentImg = 1;
		jugar = false;
		for(Terreno t : Vikings.m.tierras) {
		t.currentImg = 0;
		}
		b = new Barquito("Barco", 0, 725, 300, 800, "barco.png");
		b.collisionBox(0, 50, 200, 0);
		hud = new HUD("vidas", 25, 25, 175, 75, vidas);
		m.enemigos = new ArrayList<>();
		m.tierras = new ArrayList<>();
		//m.tiros = new ArrayList<>();
		//m.tirosE = new ArrayList<>();
		trans = false;
		jugar = false;
		ins = false;
		salir = false;
		portal = false;
		apple = false;
		flag = false;
		m.enemigos.add((new Enemigos("Pato", 1850, 550, 1950, 620, "Ene.png")));
		m.enemigos.add((new Enemigos("Pato", 4150, 550, 4250, 620, "Ene.png")));
		m.enemigos.add((new Enemigos("Pato", 5850, 550, 5950, 620, "Ene.png")));
		m.enemigos.add((new Enemigos("PatoF", 7800, 400, 7950, 620, "Ene.png")));
//		m.tierras.add((new Terreno("suelo", 100, 400, 600, 470, "SueloTierra.png")));
//		m.tierras.add((new Terreno("suelo", 400, 600, 1000, 650, "SueloTierra.png")));
		m.tierras.add((new Terreno("suelo", 1000, 650, 1170, 700, suelitos)));
		m.tierras.add((new Terreno("sueloabajo", 1170, 650, 1400, 700, suelitos)));
		m.tierras.add((new Terreno("suelo", 1400, 650, 2000, 700, suelitos)));
		m.tierras.add((new Terreno("sueloabajo2", 4000, 650, 4300, 700, suelitos)));
		m.tierras.add((new Terreno("suelo", 3400, 650, 4000, 700, suelitos)));
		m.tierras.add((new Terreno("suelo", 5200, 650, 5390, 700, suelitos)));
		m.tierras.add((new Terreno("suelopincho", 5390, 650, 5400, 700, suelitos)));
		m.tierras.add((new Terreno("suelo", 5400, 650, 6000, 700, suelitos)));
		m.tierras.add((new Terreno("suelo", 7400, 650, 8000, 700, suelitos)));
		m.tierras.add((new Terreno("suelo", 8100, 650, 8300, 700, suelitos)));		
		
		
		
		while (!flag) {
			f.background = "mar.jpg";
			b.move();
			r.choque();
			Enemigos.moveE();
			Terreno.barranco();
			Terreno.chusa();
			r.update();
			//Enemigos.disparoE();
			patitoMuerto();
			input();
			Enemigos.gravedadE();
			//ProyectilE.colisionP();
			Enemigos.saltoE();
			r.marcadorV(hud);
			Terreno.nyamnyam();
			r.gravedad();
			r.RagnarTocao();
			GoPortal();
			GoApple();
//			for (Proyectil p : m.tiros) {
//				p.move(f);
//			}
//			for (ProyectilE pE : m.tirosE) {
//				pE.move(f);
//			}
			ArrayList<Sprite> sprites = new ArrayList<Sprite>();
			sprites.add(b);
			sprites.addAll(m.enemigos);
			sprites.add(r);
			sprites.addAll(m.tierras);
			//sprites.addAll(m.tiros);
			//sprites.addAll(m.tirosE);
			sprites.add(hud);
			for (Iterator<Sprite> iterator = sprites.iterator(); iterator.hasNext();) {
				Sprite s = (Sprite) iterator.next();
				if (s.delete) {
					iterator.remove();
				}
			}
			f.draw(sprites);

			Thread.sleep(20);

		}
		}



private static void patitoMuerto() {
		for(Enemigos e : m.enemigos) {
			if(Ragnar.getRagnar().getCollisionType(e).contains("d")) {
				Ragnar.getRagnar().y1-=250;
				Ragnar.getRagnar().y2-=250;
				e.Danado();
				e.Danado();
				e.Danado();
			}
		}
	}

public static void RagnarMuerto() {
	if(flag==true) {
	menu.add((new Botones("Jugar", 0, 300, 800, 600, "JUGAR.png")));
	}
}
private static void listasVacias() {
		m.enemigos = new ArrayList<>();
		m.tierras = new ArrayList<>();
		m.tiros = new ArrayList<>();
		m.tirosE = new ArrayList<>();
		menu = new ArrayList<>();
		jugar = false;
		ins = false;
		salir = false;
		portal = false;
		apple = false;
		flag = false;
		jugar2 = false;
		jugar3 = false;
		Terreno.caida=false;
		Terreno.caida2=false;
		f.resetScroll();
		minscroll = 250;
		maxscroll = 300;
		
	}
	public static void menu() throws InterruptedException {
		while (!jugar) {
			ins=false;
			f.background = "Fondo2.jpg";
			int x = f.getCurrentMouseX();
			int y = f.getCurrentMouseY();
			menu.add((new Botones("Jugar", 25, 450, 325, 500, "JUGAR.png")));
			menu.add((new Botones("Jugar", 25, 550, 325, 600, "INS.png")));

			for (Botones s : menu) {
				if (s.collidesWithPoint(x, y)) {
					if (s.path == "JUGAR.png") {
						jugar = true;
					}
					if (s.collidesWithPoint(x, y)) {
						if (s.path == "INS.png") {
							while (!ins) {
								f.clear();
								instrucciones();
							}
							
						}

					}
				}
			}
			f.draw(menu);
			Thread.sleep(50);
		}
		
	}

	private static void instrucciones() throws InterruptedException {
			f.background = "Lobby.png";
			ArrayList<Sprite> instru = new ArrayList<>();
			instru.add((new Sprite("Jugar", 25, 550, 325, 600, "ATRAS.png")));
			int x = f.getCurrentMouseX();
			int y = f.getCurrentMouseY();
			for (Sprite s : instru) {
				if (s.collidesWithPoint(x, y)) {
					if (s.path == "ATRAS.png") {
						ins=true;

					}
				}
			}
			f.draw(instru);
			Thread.sleep(50);
		}

	public static void jugar() throws InterruptedException {
		Ragnar r = Ragnar.getRagnar();
		r.x1= 100;
		r.x2=250;
		r.y1=450;
		r.y2=600;
		r.hp=3;
		r.useImgArray = true;
		hud.useImgArray= true;
//		w.playMusic("");
		r.currentImg = 1;
		jugar = false;
		r.currentImg = 1;
		
		hud = new HUD("vidas", 25, 25, 175, 75, vidas);
		m.enemigos = new ArrayList<>();
		m.tierras = new ArrayList<>();
		m.tiros = new ArrayList<>();
		m.tirosE = new ArrayList<>();
		trans = false;
		jugar = false;
		ins = false;
		salir = false;
		portal = false;
		flag = false;
		portal=false;
		m.enemigos.add((new Enemigos("Enemigo", 1700, 100, 1800, 300, "baile.png")));
		m.enemigos.add((new Enemigos("EnemigoF", 2500, 200, 2700, 530, "baile.png")));
		m.tierras.add((new Terreno("suelo", 100, 600, 800, 670, suelitos)));
		m.tierras.add((new Terreno("suelo", 400, 530, 600, 600, suelitos)));
		m.tierras.add((new Terreno("suelo", 600, 530, 800, 600, suelitos)));
		m.tierras.add((new Terreno("suelo", 600, 470, 800, 530, suelitos)));
		m.tierras.add((new Terreno("suelo", 1000, 300, 1900, 370, suelitos)));
		m.tierras.add((new Terreno("suelo", 2000, 530, 3000, 600, suelitos)));
		
		while (!flag) {
			f.background = "fondo.jpg";
			Enemigos.moveE();
			r.update();
			Enemigos.disparoE();
			input();
			Enemigos.gravedadE();
			ProyectilE.colisionP();
			Enemigos.saltoE();
			r.marcadorV(hud);
			if (trans) {
				r.SinGravedad();
			} else {
				r.gravedad();
			}
			r.RagnarTocao();
			GoPortal();
			for (Proyectil p : m.tiros) {
				p.move(f);
			}
			for (ProyectilE pE : m.tirosE) {
				pE.move(f);
			}
			ArrayList<Sprite> sprites = new ArrayList<Sprite>();
			sprites.addAll(m.enemigos);
			sprites.add(r);
			sprites.addAll(m.tierras);
			sprites.addAll(m.tiros);
			sprites.addAll(m.tirosE);
			sprites.add(hud);
			for (Iterator iterator = sprites.iterator(); iterator.hasNext();) {
				Sprite s = (Sprite) iterator.next();
				if (s.delete) {
					iterator.remove();
				}
			}
			f.draw(sprites);

			Thread.sleep(20);

		}
		}
	
	
	
	private static void GoPortal() {
		for(Enemigos e : m.enemigos) {
			if(e.delete) {
			if(e.name=="EnemigoF" && e.delete) {
				if(!portal) {
				portal();
				}
			}
		}
		}
		for(Terreno t : m.tierras) {
			if(t.name=="portal" && Ragnar.getRagnar().collidesWith(t)) {
				jugar=false;
				flag=true;
				jugar2=true;
				
			}	
			
		}
	}
	private static void GoApple() {
		for(Enemigos e : m.enemigos) {
			if(e.delete) {
			if(e.name=="PatoF" && e.delete) {
				if(!apple) {
				apple();
				}
			}
		}
		}
		}
	private static void apple() {
		for(Enemigos e : m.enemigos) {
		m.tierras.add((new Terreno("apple", 8150, 600, 8250, 650, "calabaza.png")));
		apple=true;	
		}
		
	}
	private static void portal() {
		m.tierras.add((new Terreno("portal", 2800, 200, 3000, 530, "valhalla.jpg")));
		portal=true;
		
	}
	/**
	 * Toda la lista de movimientos y acciones que puede hacer nuestro personaje
	 */
	private static void input() {
		// TODO Auto-generated method stub
		// devuelve un set con todas las teclas apretadas
		if (w.getPressedKeys().contains('a')) {
			Ragnar.getRagnar().currentImg = 2;
			Ragnar.getRagnar().moveIzq(f);
			if (Ragnar.getRagnar().x1 < minscroll) {
				
				// scroll positiu: A la dreta.
				f.scroll(5, 0);
				// has d'ajustar minscroll i maxscroll de la mateixa forma
				minscroll -= 5;
				maxscroll -= 5;
			}
		} else if (w.getPressedKeys().contains('d')) {
			Ragnar.getRagnar().currentImg = 1;
			Ragnar.getRagnar().moveDer(f);
			if (Ragnar.getRagnar().x1 > maxscroll) {
				// scroll positiu: A l'esquerra
				f.scroll(-5, 0);
				// has d'ajustar minscroll i maxscroll de la mateixa forma
				minscroll += 5;
				maxscroll += 5;
			}
		}

		if (w.getPressedKeys().contains('w')) {
			if (Ragnar.getRagnar().isOnCeiling(f)) {

			} else {
				Ragnar.getRagnar().jump(18, 15);
			}
		}
		if (w.getPressedKeys().contains(' ')) {
			Proyectil p = (Ragnar.getRagnar().shoot());
			if (p != null) {
				m.tiros.add(p);
				p.currentImg = 1;
			}

		}
		if (w.getPressedKeys().contains('p')) {
			Ragnar.getRagnar().espadazo();

			for (Enemigos enemigos : m.enemigos) {
				if (Ragnar.getRagnar().collidesWith(enemigos)) {
					enemigos.Danado();
				}
			}
		} else if (w.getPressedKeys().contains('l')) {
			Ragnar.getRagnar().currentImg = 5;
			Ragnar.getRagnar().RagnarTocao();

		} else if (w.getPressedKeys().contains('r')) {
			Proyectil pR = (Ragnar.getRagnar().shootR());
			if (pR != null) {
				m.tiros.add(pR);
				pR.currentImg = 2;
			}

		} else if (w.getPressedKeys().contains('o')) {
			if (w.getKeyPressed() == 'o') {
				try {
					Thread.sleep(100);
				} catch (InterruptedException e) {

					e.printStackTrace();
				}
				Ragnar.getRagnar().tranformacion();
			
			}
		} else if (w.getPressedKeys().contains('s')) {
			Ragnar.getRagnar().pabajo();
		} else if (!w.getPressedKeys().contains('s')) {
			Ragnar.getRagnar().parriba();
		}
	}

}