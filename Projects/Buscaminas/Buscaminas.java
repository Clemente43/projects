import java.util.Random;
import java.util.Scanner;

public class Buscaminas {
	static Random r = new Random();
	static Scanner sc = new Scanner(System.in);
	static int[][] tablero;
	static int[][] tableroMinas;
	static int fila;
	static int columna;
	static int minas;

	public static void main(String[] args) {
		init();
		boolean exit = false;
		while (!exit) {
			System.out.println("Escribe opcion: 1 2 3 4");
			int menu = sc.nextInt();
			switch (menu) {
			case 1:
				MostrarAyuda();
				break;
			case 2:
				Opciones();
				break;
			case 3:
				JugarPartida();
				break;
			case 4:
				Ranking();
				break;
			case 0:
				exit = true;
				break;
			default:
				System.out.println("Aborto de Prolog");
			}
		}
	}

	private static void viewMinas() {
		for (int f = 0; f < tableroMinas.length; f++) {
			for (int c = 0; c < tableroMinas[0].length; c++) {
				System.out.print(tableroMinas[f][c] + " ");
			}
			System.out.println();
		}
			System.out.println("-------------------------------");
		}
	private static void init() {

	}

	private static void initCamp() {
		tablero = new int[fila][columna];
		for (int f = 0; f < tablero.length; f++) {
			for (int c = 0; c < tablero[0].length; c++) {
				tablero[f][c] = 9;
			}
		}

	}

	private static void initMinas() {
		tableroMinas = new int[fila][columna];
		int i = 0;
		while (i < minas) {
			int rx = r.nextInt(fila);
			int ry = r.nextInt(columna);
			if (tableroMinas[rx][ry] == 0) {
				tableroMinas[rx][ry] = 1;
				i++;
			}
		}
	}

	private static void Ranking() {

	}

	private static void JugarPartida() {
		initMinas();
		initCamp();
		viewMinas();
		viewCamp();
	}

	private static void viewCamp() {
		for (int f = 0; f < tablero.length; f++) {
			for (int c = 0; c < tablero[0].length; c++) {
				System.out.print(tablero[f][c] + " ");
			}
			System.out.println();
		}
	}
	private static void Opciones() {
		System.out.println("Nombre:");
		String nombre = sc.nextLine();
		sc.nextLine();
		System.out.println("Tablero:");
		fila = sc.nextInt();
		columna = sc.nextInt();
		sc.nextLine();
		System.out.println("Nombre de mines:");
		minas = sc.nextInt();
	}


	private static void MostrarAyuda() {

	}

}
