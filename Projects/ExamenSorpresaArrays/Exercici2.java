import java.util.HashSet;
import java.util.Scanner;
import java.util.Random;

public class Exercici2 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner sc = new Scanner(System.in);
		int opcion = 0;
		HashSet<String> set = new HashSet<String>();
		String pokemon = "";
		Random r = new Random();
		boolean sortir = false;
		while (!sortir) {
			System.out.println("Escoge una opcio: 1 Entregar Pokemon, 2 Recollir Pokemon, 3 Salir");
			opcion = sc.nextInt();
			sc.nextLine();
			switch (opcion) {
			case 1:
				pokemon = sc.nextLine();
				set.add(pokemon);
				System.out.println("Tu pokemon ha sido añadido " + set);
				break;
			case 2:
				boolean huevo = r.nextBoolean();
				pokemon = sc.nextLine();
				set.remove(pokemon);
				System.out.println("Tu pokemon ha sido retirado " + set);
				if(huevo)
					System.out.println("Toma huevo");
				break;
			case 3:
				sortir = true;
				break;
			default:
				System.out.println("Aborto de Prolog");

			}
		}
	}
}
