
public class Motos extends Vehiculos{

	public Motos(String matricula, String model, int anys, String situacion, int cilindrada) {
		super(matricula, model, anys, situacion, cilindrada);
		tipo = 'm';

	}

	@Override
	public void repara() {
		if(this.getSituacion().equals("Disponible")) {
			if(this.anys>2) {
				this.setSituacion("Baixa");
			}
			this.setSituacion("Avariada");
		}
	}

}
