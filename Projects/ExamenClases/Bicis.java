
public class Bicis extends Vehiculos{

	public Bicis(String matricula, String model, int anys, String situacion) {
		super(matricula, model, anys, situacion);
		tipo = 'b';

	}

	@Override
	public void repara() {
		if(this.getSituacion().equals("Disponible")) {
			this.setSituacion("Avariada");
		}
	}

}
