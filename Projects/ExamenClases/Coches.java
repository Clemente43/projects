
public class Coches extends Vehiculos{

	public Coches(String matricula, String model, int places, int anys, String situacion) {
		super(matricula, model, places, anys, situacion);
		tipo = 'c';
	}
	
	@Override
	public void repara() {
		if(this.getSituacion().equals("Disponible")) {
			if(this.anys>3) {
				this.setSituacion("Baixa");
			}
			this.setSituacion("Avariada");
		}
	}

	
}
