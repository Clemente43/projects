
public abstract class Vehiculos {
	
	public String getSituacion() {
		return situacion;
	}
	public void setSituacion(String situacion) {
		this.situacion = "Disponible";
		this.situacion = "Llogada";
		this.situacion = "Avariada";
		this.situacion = "Baixa";
	}
	
	String matricula;
	String model;
	int places;
	int anys;
	String situacion;
	int cilindrada;
	char tipo;
	public Vehiculos(String matricula, String model, int places, int anys, String situacion) {
		super();
		this.matricula = matricula;
		this.model = model;
		this.places = places;
		this.anys = anys;
		this.situacion = situacion;
	}
	public Vehiculos(String matricula, String model, int anys, String situacion, int cilindrada) {
		super();
		this.matricula = matricula;
		this.model = model;
		this.anys = anys;
		this.situacion = situacion;
		this.cilindrada = cilindrada;
	}
	public Vehiculos(String matricula, String model, int anys, String situacion) {
		super();
		this.matricula = matricula;
		this.model = model;
		this.anys = anys;
		this.situacion = situacion;
	}
	public void baja() {
		if(this.anys>4) {
			this.setSituacion("Baixa");
		}
	}
	@Override
	public String toString() {
		return "Vehiculos [matricula=" + matricula + ", model=" + model + ", places=" + places + ", anys=" + anys
				+ ", situacion=" + situacion + ", cilindrada=" + cilindrada + "]";
	}
	public  Vehiculos clonar(Vehiculos v){
		return v;
	}
	public boolean lloga() {
		if(this.getSituacion().equals("Llogada")) {
		return false;
		}else {
		return true;
		}
	}
	public void retorna() {
		this.setSituacion("Disponible");
	}
	public abstract void repara();
}
