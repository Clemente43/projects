

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.Random;

public class TestSerialList {

	public static void main(String[] args) throws IOException, ClassNotFoundException {

		ArrayList<Emu> emus = new ArrayList<>();
		File f = new File("resources/emus.list");
		FileOutputStream fos = new FileOutputStream(f);
		ObjectOutputStream oos = new ObjectOutputStream(fos);

		Random r = new Random();

		for (int i = 0; i < 10; i++) {
			if (r.nextBoolean()) {
				Emu e = new Emu(r.nextDouble(), r.nextInt(10), true, 0);
				emus.add(e);
			}
		}

		oos.writeObject(emus);
		oos.flush();
		// CERRAR ANTES DE VOLVER A ABRIR
		oos.close();

		File f2 = new File("resources/emus.list");
		FileInputStream fis = new FileInputStream(f2);
		ObjectInputStream ois = new ObjectInputStream(fis);

		Object o = ois.readObject();
		ArrayList<Emu> list = (ArrayList<Emu>) o;
		System.out.println(list);

	}

}
