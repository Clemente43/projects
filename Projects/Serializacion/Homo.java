import java.io.Serializable;

public class Homo implements Serializable{
	
	double altura;
	int edad;
	boolean vida;
	
	public Homo(double altura, int edad, boolean vida) {
		super();
		this.altura = altura;
		this.edad = edad;
		this.vida = vida;
	}

	@Override
	public String toString() {
		return "Homo [altura=" + altura + ", edad=" + edad + ", vida=" + vida + "]";
	}
	
}
