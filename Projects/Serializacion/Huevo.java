

import java.io.Serializable;
import java.util.Random;

public class Huevo implements Serializable {
	
	double tamano;
	String color;
	Random r = new Random();
	
	public Huevo() {
		tamano = r.nextDouble();
		color = r.nextBoolean()? "negro":"azul";
	}
	@Override
	public String toString() {
		return "Huevo [tama�o=" + tamano + ", color=" + color + "]";
	}
	
	
	

}
