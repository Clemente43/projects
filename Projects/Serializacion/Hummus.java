

import java.io.Serializable;

public class Hummus implements Serializable{
	
	boolean rico;
	int nGarbanzos;
	String marca;
	
	public Hummus(int nGarbanzos, String marca) {
		super();
		this.nGarbanzos = nGarbanzos;
		this.marca = marca;
		this.rico=true;
	}

	@Override
	public String toString() {
		return "Hummus [rico=" + rico + ", nGarbanzos=" + nGarbanzos + ", marca=" + marca + "]";
	}
	
	
	

}
