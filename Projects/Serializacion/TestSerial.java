

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.Random;

public class TestSerial {

	public static void main(String[] args) throws IOException, ClassNotFoundException {

		// Emu e = new Emu(1.2, 4, true, 0);

		File f = new File("resources/emus.alcantarillo");
		FileOutputStream fos = new FileOutputStream(f);
		ObjectOutputStream oos = new ObjectOutputStream(fos);

		Random r = new Random();
		
		for (int i = 0; i < 10; i++) {
			r.nextInt(2);
			if (r.nextInt(2)==0) {
				Emu e = new Emu(r.nextDouble(), r.nextInt(10), true, 0);
				oos.writeObject(e);
			} else if (r.nextInt(2)==1){
				Hummus h = new Hummus(r.nextInt(1000), "hacendado");
				oos.writeObject(h);
			}else {
				Homo ho = new Homo(r.nextDouble(), r.nextInt(100), r.nextBoolean());
				oos.writeObject(ho);
			}
		}
		// el buffer se escribe cuando esta un poco rellenito.
		oos.flush();
		// CERRAR ANTES DE VOLVER A ABRIR
		oos.close();

		File f2 = new File("resources/emus.alcantarillo");
		FileInputStream fis = new FileInputStream(f2);
		ObjectInputStream ois = new ObjectInputStream(fis);

		for (int i = 0; i < 10; i++) {
			Object o = ois.readObject();
			if(o instanceof Emu) {
				Emu e2 = (Emu) o;
				System.out.println(e2);
			}else if(o instanceof Hummus) {
				Hummus e2 = (Hummus) o;
				System.out.println(e2);
			}else if(o instanceof Homo) {
				Homo e2 = (Homo) o;
				System.out.println(e2);
			}
			
		}

	}

}
