

import java.io.Serializable;

public class Emu implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -3L;
	
	double altura;
	int edad;
	boolean vida;
	int balasDodgeadas;
	Huevo egg;
	
	public Emu(double altura, int edad, boolean vida, int balasDodgeadas) {
		super();
		this.altura = altura;
		this.edad = edad;
		this.vida = vida;
		this.balasDodgeadas = balasDodgeadas;
		egg = new Huevo();
	}

	@Override
	public String toString() {
		return "Emu [altura=" + altura + ", edad=" + edad + ", vida=" + vida + ", balasDodgeadas=" + balasDodgeadas
				+ ", egg=" + egg + "]";
	}


	
	
	

}
