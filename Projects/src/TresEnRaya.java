import java.util.Scanner;

public class TresEnRaya {

	// variable global: se usa en mas de una funcion, se declara fuera y estatica
	static char[][] tablero;
	static boolean turnob;
	static int turnoi;
	static char turnoc;
	static String turnos;
	public static void main(String[] args) {

		// siempre vamos a tener una funcion de inicializacion
		init();
		// un turno
		int arcaico=3;
		while (arcaico == 3) {
			ponerFicha(turnos);
			arcaico = comprobarVictoria();
			turnob = cambiarTurnob(turnob);
			//turnoi = cambiarTurnoi(turnoi);
			//turnoc = cambiarTurnoc(turnoc);
			//turnos = cambiarTurnos(turnos);
		}

	}

	private static char[][] init() {
		tablero = new char [3][3];
		for (int f = 0; f < tablero.length; f++) {
			for (int c = 0; c < tablero[0].length; c++) {
				tablero[f][c]= '·';
			}
		}
		return tablero;
		
	}

	private static void ponerFicha(String turnos2) {
		Scanner sc = new Scanner(System.in);
		System.out.println("Pon una ficha. Escribe las cordenadas");
		int fila=sc.nextInt();
		int columna=sc.nextInt();
		for (int f = 0; f < tablero.length; f++) {
			for (int c = 0; c < tablero[0].length; c++) {
				tablero[fila][columna]= 'simbolodelturno';
			}
		}
	}

	private static String cambiarTurnos(String turnos2) {
		if(turnos2.equals("卐")) {
			return "✡";
		}else {
			return "卐";
		}
	}

	private static char cambiarTurnoc(char turnoc2) {
		if(turnoc2=='X') {
			return 'O';
		}else {
			return 'X';
		}
	}

	private static int cambiarTurnoi(int turnoi2) {
		return (turnoi2+1)%2;
	}

	private static boolean cambiarTurnob(boolean turnob2) {
		return !turnob;
	}

	private static int comprobarVictoria() {
		
		return 0;
	}

}
