
public class Recursividad {
	public static void main(String[] args) {
		int fa = 5;
		int res= sumatorio(fa);
		System.out.println(res);
	}
	private static int sumatorio(int fa) {

		if (fa == 1) {
			// caso base
			return 1;
		} else {

			// caso recursivo
			return fa + sumatorio(fa - 1);
		}
	}

}
