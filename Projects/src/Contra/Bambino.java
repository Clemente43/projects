package Contra;

public class Bambino extends Sprite {

	char direccion='d';
	int cooldown=25; 
	int estat = 0;  //0 suelo, 1 subiendo  2 cayendo
	int maxSalto =50;
	public Bambino(String name, int x1, int y1, int x2, int y2, String path) {
		super(name, x1, y1, x2, y2, path);
		// TODO Auto-generated constructor stub
	}

	public void moveIzq() {
		x1--; x2--;
		direccion='i';
	}

	public void moveDer() {
		// TODO Auto-generated method stub
		x1++;x2++;
		direccion='d';
	}

	public void movv(Field f) {
		
		if(estat==1) {
			y1--;y2--;
			maxSalto--;
			if(maxSalto==0) {
				estat=2;
			}
		}
		else if(isGrounded(f)) {
			estat = 0;
			maxSalto=50;
		}else {
			estat = 2;
			y1++;y2++;
			if(y2==700) {
				System.out.println("rip");
				System.exit(0);
			}
		}
		
	}

	public void jump() {
		if(estat == 0) {
			estat = 1;
		}
	}

	public Proyectil shoot() {
		// TODO Auto-generated method stub
		if(cooldown==25) {
			cooldown=0;
			Proyectil p = new Proyectil("Proyectil", (x1+x2)/2, (y1+y2)/2, ((x1+x2)/2)+500, ((y1+y2)/2)+500, "soylabomba.png", direccion);
			return p;
		}else {
			return null;
		}
		
	}
	
	public void update() {
		if(cooldown<25)cooldown++;
	}

}
