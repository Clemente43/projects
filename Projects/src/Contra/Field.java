package Contra;

import java.awt.*;
import java.awt.event.*;
import java.awt.geom.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import javax.imageio.ImageIO;
import javax.swing.*;

public class Field extends JPanel {

	ArrayList<Sprite> sprites = new ArrayList<>();
	boolean recentDraw=false;

	/**
	 * Event handler every time mouse is clicked.
	 */
	private MouseListener ml = new MouseAdapter() {
		// TODO
	};

	/**
	 * Redraws field if window size is changed.
	 */
	ComponentListener cl = new ComponentAdapter() {
		public void componentResized(ComponentEvent e) {
			//TODO
			repaint();
		}
	};

	public Field() {
		//setFocusable(true);
		addMouseListener(ml);

	}

	public void add(ArrayList<? extends Sprite> newSprites) {
		if(recentDraw) {
			clear();
			recentDraw=false;
		}
		sprites.addAll(newSprites);
	}
	
	public void add(Sprite newSprites) {
		if(recentDraw) {
			clear();
			recentDraw=false;
		}
		sprites.add(newSprites);
	}

	public void clear() {
		sprites.clear();
	}

	public void draw() {
		// TODO
		recentDraw = true;
		repaint();
	}
	
	public void draw(ArrayList<? extends Sprite> sprites2) {
		clear();
		sprites.addAll(sprites2);
		repaint();
	}
	
	public void draw(Sprite s2) {
		sprites.clear();
		sprites.add(s2);
		repaint();
		
	}

	@Override
	public void paintComponent(Graphics g) {
		super.paintComponent(g);

		Graphics2D g2d = (Graphics2D) g;

		g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);

		g2d.setRenderingHint(RenderingHints.KEY_RENDERING, RenderingHints.VALUE_RENDER_QUALITY);


		//System.out.println("painting "+sprites);
		for (int i = 0; i < sprites.size(); i++) {
			drawSprite(sprites.get(i),g2d);
		}
		Toolkit.getDefaultToolkit().sync();
	}

	private void drawSprite(Sprite sprite, Graphics2D g2d) {
		


		int x = sprite.x2-sprite.x1;
		int y = sprite.y2-sprite.y1;

		Image img;
		try {
			
			g2d.drawImage(sprite.img, sprite.x1, sprite.y1, x, y, this);

		} catch (Exception e) {
			System.out.println("Error on image " + sprite.img + " object: " + sprite.name);
			e.printStackTrace();
		}
	}

	public ArrayList<Sprite> getSprites() {
		return sprites;
	}

	public void setSprites(ArrayList<Sprite> sprites) {
		this.sprites = sprites;
	}

	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	

	

}
