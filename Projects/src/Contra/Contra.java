package Contra;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.Random;

public class Contra {
	
	
	//static ArrayList<Murciano> murcianos = new ArrayList<>();
	static Field f = new Field();
	static Window w = new Window(f);
	static Bambino b = new Bambino("Bambino", 50, 50, 100, 100, "baile.png");
	//static Davilillo d = new Davilillo("Davilillo", 350, 300, 400, 350, "davi.gif");
	static ArrayList<Terreno> tierras = new ArrayList<>();
	static ArrayList<Proyectil> tiros = new ArrayList<>();
	
	public static void main(String[] args) throws InterruptedException {
		
		tierras.add((new Terreno("suelo", 100, 300, 300, 320, "b84.png")));
		tierras.add((new Terreno("suelo", 400, 300, 500, 320, "b84.png")));
		int contador =  0;
		boolean flag=false;
		while(!flag) {
			b.update();
			input();
			gravedad();
			for(Proyectil p : tiros) {
				p.move();
			}
			ArrayList<Sprite> sprites = new ArrayList<Sprite>();
			sprites.add(b);
			sprites.addAll(tierras);
			sprites.addAll(tiros);
			f.draw(sprites);
		
			Thread.sleep(20);
			
		}
		System.out.println("estas en el lobby");
		
		
	}
	private static void gravedad() {
		// TODO Auto-generated method stub
		b.movv(f);
	}
	private static void input() {
		// TODO Auto-generated method stub
		//devuelve un set con todas las teclas apretadas
		if(w.getPressedKeys().contains('a')) {
			b.moveIzq();
		}else if(w.getPressedKeys().contains('d')) {
			b.moveDer();
		}
		if(w.getPressedKeys().contains('w')) {
			b.jump();
		}
		if(w.getPressedKeys().contains(' ')) {
			Proyectil p = (b.shoot());
			if(p!=null) {
				tiros.add(p);
			}
		}
		
	}

	

}
