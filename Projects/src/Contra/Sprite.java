package Contra;

import java.awt.Image;
import java.awt.Rectangle;
import java.util.ArrayList;

import javax.swing.ImageIcon;

public class Sprite {
	
	public final int MARGIN = 4;
	
	public String name;
	
	public int x1;
	public int y1;
	
	public int x2;
	public int y2;
	
	public String path;
	public boolean solid;
	public boolean ground;
	
	
	public Image img; 
	
	public Sprite(String name, int x1, int y1, int x2, int y2, String path) {
		super();
		this.name = name;
		this.x1 = x1;
		this.y1 = y1;
		this.x2 = x2;
		this.y2 = y2;
		this.path = path;
		img = new ImageIcon((this.path)).getImage();
	}
	
	
    protected Rectangle getRect() {
        return new Rectangle(x1, y1, x2-x1, y2-y1);
    }
    
    protected Rectangle getBottomRect() {
    	return new Rectangle(x1+MARGIN, y2-MARGIN, x2-x1-MARGIN*2, MARGIN);    
    	}
    
    protected Rectangle getTopRect() {
        return new Rectangle(x1+MARGIN, y1, x2-x1-MARGIN*2, MARGIN);
    }
    
    
    public ArrayList<Sprite> collidesWithList(ArrayList<? extends Sprite> others) {
    	ArrayList<Sprite> list = new ArrayList<>();
    	for(Sprite s: others) {
    		if (this.getRect().intersects(s.getRect())) list.add(s);
    	}
    	return list;
    	
    }
    
    public boolean collidesWith(Sprite other) {
    	return (this.getRect().intersects(other.getRect()) )?  true : false;
    }
    
    public Sprite collidesInField(Field f) {
    	for (int i = 0; i < f.sprites.size(); i++) {
    		if (this.getRect().intersects(f.sprites.get(i).getRect())) return f.sprites.get(i);
		}
    	return null;    }
    
    
    public Sprite firstCollidesWithList(ArrayList<? extends Sprite> others) {
    	for(Sprite s: others) {
    		if (this.getRect().intersects(s.getRect())) return s;
    	}
    	return null;
    	
    }
    
    
    public boolean stepsOn(Sprite other) {
    	return (this.getBottomRect().intersects(other.getTopRect()) )?  true : false;
    }
    
    public boolean isGrounded(Field f) {
    	for (int i = 0; i < f.sprites.size(); i++) {
			if(f.sprites.get(i).ground&&this.stepsOn(f.sprites.get(i))) return true;
		}
    	return false;
    }
	
	
	

}
