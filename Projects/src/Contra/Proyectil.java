package Contra;


public class Proyectil extends Sprite {

	char direccion;
	
	public Proyectil(String name, int x1, int y1, int x2, int y2, String path) {
		super(name, x1, y1, x2, y2, path);
		// TODO Auto-generated constructor stub
	}

	public Proyectil(String name, int x1, int y1, int x2, int y2, String path, char dir) {
		super(name, x1, y1, x2, y2, path);
		direccion = dir;
		
	}
	
	public void move() {
		if(direccion=='i') {
			x1--;x2--;
		}else {
			x1++;x2++;
		}
	}

}
