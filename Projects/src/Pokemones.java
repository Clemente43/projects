import java.util.Random;

public class Pokemones {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		Random r = new Random();
		
		
		
		int atac = r.nextInt(31)+1;
		int def = r.nextInt(31)+1;
		int vel = r.nextInt(31)+1;
		int spc = r.nextInt(31)+1;
		
		String tipus1 = "Fuego";
		String tipus2 = "Dragon";
		
		int npokedex = r.nextInt(151)+1;
		
		boolean Shiny = r.nextBoolean();
		
		
		//A. Media mas grande de 20
		double media = (atac+def+vel+spc)/4;
		if (media>=20) {
			System.out.println("Se cumple A");
		}else {
				System.out.println("No se cumple A");
		}
		//B. Todo mas grande que 16
		if (atac>=16&&def>=16&&vel>=16&&spc>=16) {
			System.out.println("Se cumple B");
		}else {
			System.out.println("No se cumple B");
		}
		//C. Especial tipo fuego no inicial
		if (tipus1=="Fuego" && npokedex>=133 && npokedex<=143){
			System.out.println("Se cumple C");
		}else {
			System.out.println("No se cumple C");
		}
		//D. Tipo Hielo
		if (tipus1=="Hielo"|| tipus2=="Hielo") {
			System.out.println("Se cumple D");
		}else {
			System.out.println("No se cumple D");
		}
		//E. Solo tipo Dragon
		if (tipus1=="Dragon" && tipus2==null) {
			System.out.println("Se cumple E");
		}else {
			System.out.println("No se cumple E");
		}
		//F. Tipo Veneno y Spc>18
		if ((tipus1=="Veneno" || tipus2=="Veneno") && spc>18) {
			System.out.println("Se cumple F");
		}else {
			System.out.println("No se cumple F");
		}
		//G.Tipo Lucha y Atac>Defensa
		if ((tipus1=="Lucha" || tipus2=="Lucha") && atac>def) {
			System.out.println("Se cumple G");
		}else {
			System.out.println("No se cumple G");
		}
		//H. Tipo Acero y atac y def mayores a 25 o especial superior a 27. No legendarios
		if ((tipus1=="Acero" || tipus2=="Acero") && (atac>25&&def>25 || spc>27) && (npokedex<144)) {
			System.out.println("Se cumple H");
		}else {
		System.out.println("No se cumple H");
		//I. Tipo Acero y atac y def mayores a 25 o especial superior a 27. No legendarios
		if ((tipus1=="Acero" || tipus2=="Acero") && (atac>25&&def>25 || spc>27) && (npokedex<144) || (Shiny ==true)) {
			System.out.println("Se cumple I");
		}else {
		System.out.println("No se cumple I");
		}
		}
	}
}

