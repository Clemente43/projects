package MatricesChulas;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.Random;
import java.util.Scanner;

public class Simon3x3 {

	public static void main(String[] args) throws InterruptedException {

		int[][] simon = { { 1, 2, 3 }, { 4, 5, 6 }, { 7, 8, 9 }};

		Board b = new Board();
		Window w = new Window(b);

		b.setActcolors(true);
		int[] colors = { 0xFFFFFF, 0x11851f, 0x9a1738, 0x939a17, 0x17269a, 0x982e74, 0x7f2f94, 0xba7621, 0x870231, 0x5a5a5a, 0x22fb00, 0xfb0000, 0xf9fb00, 0x00dbfb, 0xff00a7, 0xc900ff, 0xff9a00, 0xff005a, 0xa9a9a9 };
		b.setColors(colors);

		// 1: verdeA 2: rojoA 3: amarilloA 4: azulA 5:RosaA 6:MoradoA 7:NaranjaA 8:GranateA 9:GrisA
		// 10: verdeE 11: rojoE 12: amarilloE 13: azulE 14:RosaE 15:MoradoE 16:NaranjaE 17:GranateE 18:GrisE 
		/// Simon dice

		Utils.view(simon);
		b.draw(simon);
		Scanner sc = new Scanner(System.in);
		Random r = new Random();
		ArrayList<Integer> repes = new ArrayList<Integer>();
		boolean loser = false;

		while (!loser) {
			int colorrandom = r.nextInt(9) + 1;
			repes.add(colorrandom);

			for (int color : repes) {

				int x, y;
				switch (color) {
				case 1:
					x = 0;
					y = 0;
					break;
				case 2:
					x = 0;
					y = 1;
					break;
				case 3:
					x = 0;
					y = 2;
					break;
				case 4:
					x = 1;
					y = 0;
					break;
				case 5:
					x = 1;
					y = 1;
					break;
				case 6:
					x = 1;
					y = 2;
					break;
				case 7:
					x = 2;
					y = 0;
					break;
				case 8:
					x = 2;
					y = 1;
					break;
				case 9:
					x = 2;
					y = 2;
					break;
				default:
					x = 0;
					y = 0;

				}

				// eso es lo mismo que simon[x][y] = simon[x][y]+4;
				simon[x][y] += 9;
				Utils.view(simon);
				b.draw(simon);
				Thread.sleep(750);
				simon[x][y] -= 9;
				b.draw(simon);
				Thread.sleep(200);

			}

			System.out.println("ahora juega el jugador");

			int aciertos = 0;
			while (aciertos<repes.size()&&!loser) {
				Thread.sleep(50);
				int row = b.getCurrentMouseRow();
				int col = b.getCurrentMouseCol();
				if (row != -1 && col != -1) {
					System.out.println(row + " " + col);
					int jug=0;
					if(row==0&&col==0) jug = 1;
					if(row==0&&col==1) jug = 2;
					if(row==0&&col==2) jug = 3;
					if(row==1&&col==0) jug = 4;
					if(row==1&&col==1) jug = 5;
					if(row==1&&col==2) jug = 6;
					if(row==2&&col==0) jug = 7;
					if(row==2&&col==1) jug = 8;
					if(row==2&&col==2) jug = 9;
					if (repes.get(aciertos) != jug) {
						System.out.println("loser");
						loser = true;
						break;
					}else {
						aciertos++;
					}
					
					if (b.getCurrentMouseCol() == 2)
						break;
				}
			}

			
		}

		System.out.println("has perdido en la ronda " + repes.size());

	}

}
