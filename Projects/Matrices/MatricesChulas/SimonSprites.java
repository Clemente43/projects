package MatricesChulas;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.Random;
import java.util.Scanner;

public class SimonSprites {

	public static void main(String[] args) throws InterruptedException {

		int[][] simon = { { 1, 2 }, { 3, 4 } };

		Board b = new Board();
		Window w = new Window(b);

		b.setActsprites(true);
		String[] sprites = { "", "Hitler1.jpg","franco1.jpg", "mussolini1.jpg", "kyu1.jpg","Hitler2.jpg", "franco2.jpg", "mussolini2.jpg", "kyu2.jpg"};
		b.setSprites(sprites);

		// 1: verdeA 2: rojoA 3: amarilloA 4: azulA
		// 5: verdeE 6: rojoE 7: amarilloE 8: azulE
		/// Simon dice

		Utils.view(simon);
		b.draw(simon);
		Scanner sc = new Scanner(System.in);
		Random r = new Random();
		ArrayList<Integer> repes = new ArrayList<Integer>();
		boolean loser = false;

		while (!loser) {
			int spritesrandom = r.nextInt(4) + 1;
			repes.add(spritesrandom);

			for (int color : repes) {

				int x, y;
				switch (color) {
				case 1:
					x = 0;
					y = 0;
					break;
				case 2:
					x = 0;
					y = 1;
					break;
				case 3:
					x = 1;
					y = 0;
					break;
				case 4:
					x = 1;
					y = 1;
					break;
				default:
					x = 0;
					y = 0;

				}

				// eso es lo mismo que simon[x][y] = simon[x][y]+4;
				simon[x][y] += 4;
				Utils.view(simon);
				b.draw(simon);
				Thread.sleep(750);
				simon[x][y] -= 4;
				b.draw(simon);
				Thread.sleep(200);

			}

			System.out.println("ahora juega el jugador");

			int aciertos = 0;
			while (aciertos<repes.size()&&!loser) {
				Thread.sleep(50);
				int row = b.getCurrentMouseRow();
				int col = b.getCurrentMouseCol();
				if (row != -1 && col != -1) {
					System.out.println(row + " " + col);
					int jug=0;
					if(row==0&&col==0) jug = 1;
					if(row==0&&col==1) jug = 2;
					if(row==1&&col==0) jug = 3;
					if(row==1&&col==1) jug = 4;
					if (repes.get(aciertos) != jug) {
						System.out.println("loser");
						loser = true;
						break;
					}else {
						aciertos++;
					}
					
					if (b.getCurrentMouseCol() == 2)
						break;
				}
			}

			
		}

		System.out.println("has perdido en la ronda " + repes.size());

	}

}
