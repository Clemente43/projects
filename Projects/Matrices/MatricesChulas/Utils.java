package MatricesChulas;

import java.util.Random;
import java.util.Scanner;

public class Utils {
	
	public static void view(int[][] tablero) {
		for (int f = 0; f < tablero.length; f++) {
			for (int c = 0; c < tablero[0].length; c++) {
				System.out.print(tablero[f][c]+" ");
			}System.out.println();
		}
		System.out.println("-------------------------------");
	}
	
	
	public void view2(int[][] tablero) {
		for (int f = 0; f < tablero.length; f++) {
			for (int c = 0; c < tablero[0].length; c++) {
				System.out.print(tablero[f][c]+" ");
			}System.out.println();
		}
		System.out.println("-------------------------------");
	}
	
	
	public static int[][] rellenarAMano(int fil, int col){
		int[][] tablero = new int[fil][col];
		Scanner sc = new Scanner(System.in);
		for (int f = 0; f < tablero.length; f++) {
			for (int c = 0; c < tablero[0].length; c++) {
				System.out.println("escribe numero para la fila "+f+" columna "+c);
				tablero[f][c]=sc.nextInt();
			}
		}
		return tablero;
		
	}
	
	
	public static int[][] rellenarRandom(int fil, int col, int rangeInf, int rangeSup){
		int[][] tablero = new int[fil][col];
		Random rRrRr = new Random();
		for (int f = 0; f < tablero.length; f++) {
			for (int c = 0; c < tablero[0].length; c++) {
				tablero[f][c] = rRrRr.nextInt(rangeSup-rangeInf)+rangeInf;
			}
		}
		return tablero;
		
	}
	
	public static int[] sumarFiles(int[][] tablero) {
		int acc = 0;
		int[] sumaDeFiles = new int[tablero.length];
		for (int f = 0; f < tablero.length; f++) {
			for (int c = 0; c < tablero[0].length; c++) {
				acc+= tablero[f][c];
			}
			sumaDeFiles[f]=acc;
			acc=0;
		}
		return sumaDeFiles;
	}
	
	
	public static int[] sumarColumnes(int[][] tablero) {
		int acc = 0;
		int[] sumaDeColumnes = new int[tablero[0].length];
		for (int c = 0; c < tablero[0].length; c++) {
			for (int f = 0; f < tablero.length; f++) {
				acc+= tablero[f][c];
			}
			sumaDeColumnes[c]=acc;
			acc=0;
		}
		return sumaDeColumnes;
	}
	
	
	public static int sumarTot(int[][] tablero) {
		int acc = 0;
		for (int c = 0; c < tablero[0].length; c++) {
			for (int f = 0; f < tablero.length; f++) {
				acc+= tablero[f][c];
			}
		}
		return acc;
	}
	
	
	public static int miraQueHayArriba(int[][] tablero, int x, int y) {
		return tablero[x-1][y];
		
	}
	
	public static int[][] borde(int[][] tablero){
		int[][] mborde = new int[tablero.length+2][tablero[0].length+2];
		for (int f = 0; f < mborde.length; f++) {
			for (int c = 0; c < mborde[0].length; c++) {
				if(f==0||f==mborde.length-1||c==0||c==mborde[0].length-1) {
					mborde[f][c]=0;
				}else {
					mborde[f][c] = tablero[f-1][c-1];
				}
			}
		}
		return mborde;
	}
	
	
	public static boolean miraSiTeRodeaUn2(int[][] tablero, int x, int y) {
		if(tablero[x-1][y-1]==2||tablero[x-1][y]==2||tablero[x+1][y]==2||tablero[x][y-1]==2||tablero[x][y+1]==2||tablero[x+1][y-1]==2||tablero[x+1][y]==2||tablero[x+1][y+1]==2) {
			return true;
		}else {
			return false;
		}
	}
	

}
