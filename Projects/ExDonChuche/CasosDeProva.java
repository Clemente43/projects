
import static org.junit.Assert.assertEquals;

import org.junit.jupiter.api.Test;

class CasosDeProva {

	@Test
	void test() {
		double s=5;
		String a = 5+" "+1;
		
		String[] arr = {"1","1","1","1","1"};
		assertEquals(a, Problema.comprobar(arr, s));
		
		
		double s2=6;
		String b = "ERROR";
		String[] arr2 = {"1","1","1","01","1","0"};
		assertEquals(b, Problema.comprobar(arr2, s2));
		
		
		double s3=7;
		String c = "ERROR";
		String[] arr3 = {"10","1","1","1","0","1","0"};
		assertEquals(c, Problema.comprobar(arr3, s3));
		
		
		double s4=3;
		String d = "ERROR";
		String[] arr4 = {"1","2","3"};
		assertEquals(d, Problema.comprobar(arr4, s4));
		 
		
		double s5=2;
		String e = 1+" "+0;
		String[] arr5 = {"1","0"};
		assertEquals(e, Problema.comprobar(arr5, s5));
		
		
		double s6=7;
		String f = "Un par mas";
		String[] arr6 = {"1","0","0","0","1","1","0"};
		assertEquals(f, Problema.comprobar(arr6, s6));
		
		
		double s7=3;
		String g = "Un par mas";
		String[] arr7 = {"1","0","0"};
		assertEquals(g, Problema.comprobar(arr7, s7));
		
		
		double s8=5;
		String h = "ERROR";
		String[] arr8 = {"1","0","0","00","1"};
		assertEquals(h, Problema.comprobar(arr8, s8));
		
		double s9=1;
		String i = 1+" "+1;
		String[] arr9 = {"1"};
		assertEquals(i, Problema.comprobar(arr9, s9));
		
		double s10=1;
		String j = "Un par mas";
		String[] arr10 = {"0"};
		assertEquals(j, Problema.comprobar(arr10, s10));
		
	}

	
}
