import java.util.Scanner;

public class DomJudge5E {

	public static void main(String[] args) {

		Scanner sc = new Scanner(System.in);
		int nveces = sc.nextInt();
		for (int i = 0; i < nveces; i++) {
			int alumnes = sc.nextInt();
			int moduls = sc.nextInt();
			int acc = 0;
			int[][] mat = new int[alumnes][moduls];
			for (int f = 0; f < mat.length; f++) {
				for (int c = 0; c < mat[0].length; c++) {
					mat[f][c] = sc.nextInt();
				}
			}
			int[] sumaDeFiles = new int[mat.length];
			for (int f = 0; f < mat.length; f++) {
				for (int c = 0; c < mat[0].length; c++) {
					acc += mat[f][c];
				}
				sumaDeFiles[f] = acc;
				acc = 0;
			}
			for (int j = 0; j < sumaDeFiles.length; j++) {
				int medias = sumaDeFiles[j] / moduls;
				System.out.println(medias);
			}
		}
	}

}
