import java.util.Scanner;

public class DomJudge5O {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		int nveces = sc.nextInt();
		for (int i = 0; i < nveces; i++) {
			int fc = sc.nextInt();
			char[][] mat = new char[fc][fc];
			for (int f = 0; f < mat.length; f++) {
				for (int c = 0; c < mat[0].length; c++) {
					if (f == 0 || f == mat.length - 1 || c == 0 || c == mat[0].length - 1) {
						mat[f][c] = 'X';
					}
				}
			}
			for (int f = 0; f < mat.length; f++) {
				for (int c = 0; c < mat[0].length; c++) {
					if (f == c) {
						mat[f][c] = 'X';
					}
				}

			}
			for (int f = 0; f < mat.length; f++) {
				for (int c = 0; c < mat[0].length; c++) {
					if ((f) + (c + 1) == fc) {
						mat[f][c] = 'X';
					}
				}
			}
			for (int f = 0; f < mat.length; f++) {
				for (int c = 0; c < mat[0].length; c++) {
					if (mat[f][c] != 'X') {
						mat[f][c] = '.';
					}
				}
			}
			for (int f = 0; f < mat.length; f++) {
				for (int c = 0; c < mat[0].length; c++) {
					System.out.print(mat[f][c] + "");
				}
				System.out.println();
			}

		}
	}
}