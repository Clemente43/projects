import java.util.Scanner;

public class DomJudge5D {

	public static void main(String[] args) {

		Scanner sc = new Scanner(System.in);
		int nveces = sc.nextInt();
		for (int i = 0; i < nveces; i++) {
			int n = sc.nextInt();
			int[][] mat = new int[n][n];
			for (int f = 0; f < mat.length; f++) {
				for (int c = 0; c < mat[0].length; c++) {
					if(f==c)
					mat[f][c] = 1;
				}
			}
			for (int f = 0; f < mat.length; f++) {	
				for (int c = 0; c < mat[0].length; c++) {
					System.out.print(mat[f][c]+" ");
				}
				System.out.println();
			}
		}
	}

}
