import java.util.Scanner;

public class Conills7C {
	static int pos;
	static int res;
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		int nveces=sc.nextInt();
		for (int i = 0; i < nveces; i++) {
		int n=sc.nextInt();
		pos=0;
		res=0;
		recurs(n);
	}
	}
		private static void recurs(int n) {
			
			if (n==res) {
				// caso base
				System.out.println(pos);
			} else {
				// caso recursivo
				pos++;
				res+=n;
				recurs(n);
			}

	}

}
