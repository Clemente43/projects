import java.util.Scanner;

public class Bitlles7A {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		int nveces=sc.nextInt();
		for (int i = 0; i < nveces; i++) {
		int filas=sc.nextInt();
		System.out.println(recurs(filas));
	}
	}
		private static int recurs(int filas) {
			
			if (filas == 1) {
				// caso base
				return 1;
			} else {
				// caso recursivo
				return filas+recurs(filas - 1);
			}
		
	}

}
