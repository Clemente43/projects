import java.util.ArrayList;
import java.util.Scanner;

public class Ex2 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner sc = new Scanner(System.in);
		int nveces = sc.nextInt();
		sc.nextLine();
		for (int i = 0; i < nveces; i++) {
			int at = sc.nextInt();
			int av = sc.nextInt();
			int tv = sc.nextInt();
			int a = ((at + av) - tv) / 2;
			int t = ((at + tv) - av) / 2;
			int v = ((av + tv) - at) / 2;
			if (a < 0 || t < 0 || v < 0) {
				System.out.println("IMPOSSIBLE");
			} else {
				System.out.println(a + " " + t + " " + v);
			}
		}
	}
}
